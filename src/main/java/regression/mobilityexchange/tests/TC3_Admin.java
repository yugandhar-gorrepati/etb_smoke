package regression.mobilityexchange.tests;

import static driverfactory.Driver.*;
import static pages.mobilityportal.ClientSelectionPage.selectCustomer;
import static pages.mobilityportal.DashboardPage.loggedInAccountNo;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyEquals;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.mobilityexchange.AnnouncementsPage;
import pages.mobilityexchange.ApplicationSettingsPage;
import static pages.mobilityexchange.ApplicationSettingsPage.*;

import static pages.mobilityexchange.AnnouncementsPage.*;
import pages.mobilityexchange.DNNPage;
import pages.mobilityexchange.DeleteProductsPage;
import static pages.mobilityexchange.DeleteProductsPage.*;
import pages.mobilityexchange.EventsPage;
import pages.mobilityexchange.LinksPage;
import static pages.mobilityexchange.LinksPage.*;

import static pages.mobilityexchange.EventsPage.*;
import static pages.mobilityexchange.DNNPage.*;
import pages.mobilityexchange.MFA;
import pages.mobilityexchange.ProdCatalogCreationPage;
import static pages.mobilityexchange.ProdCatalogCreationPage.*;
import pages.mobilityexchange.ProdCatalogMgmtPage;
import static pages.mobilityexchange.ProdCatalogMgmtPage.*;
import pages.mobilityexchange.ProductCatalogConfigPage;
import pages.mobilityexchange.PublishUnpublishPage;
import static pages.mobilityexchange.PublishUnpublishPage.*;

import static pages.mobilityexchange.ProductCatalogConfigPage.*;
import pages.mobilityexchange.RolesPage;
import static pages.mobilityexchange.RolesPage.*;
import pages.mobilityexchange.UsersPage;
import pages.mobilityexchange.WebcastsPage;

import static pages.mobilityexchange.WebcastsPage.*;

import static pages.mobilityexchange.UsersPage.*;

import pages.mobilityportal.ClientSelectionPage;
import pages.mobilityportal.DashboardPage;
import pages.mobilityportal.LoginPage;
import utilities.FileDownloader;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC3_Admin extends InitTests{   
	WebDriver driver = null;
	Driver driverObj =new Driver();
	ExtentTest test = null;
	WebDriver webdriver = null;
	
	LoginPage loginObj;
	MFA mfaObj;
	ClientSelectionPage clientSelectionObj;
	DashboardPage dashboardObj;
	DNNPage dnnpageObj;
	ProdCatalogMgmtPage catalogMgmtObj;
	ProdCatalogCreationPage catalogCreationObj;
	PublishUnpublishPage publishUnpublishObj;

	public TC3_Admin(String appName) {
		super(appName);
	}

	@BeforeTest
	public void setUp() throws Exception {
		@SuppressWarnings("unused")
		TC3_Admin obj = new TC3_Admin("MobilityExchange");
		webdriver = driverObj.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
	}
		
	public void initialize() throws Exception {		
		catalogMgmtObj = new ProdCatalogMgmtPage(driver);
		catalogCreationObj = new ProdCatalogCreationPage(driver);
		publishUnpublishObj = new PublishUnpublishPage(driver);
	}

	
	@Test(priority = 1, enabled = true)
	public void login() throws Exception {
		try {
			test = reports.createTest("Verifying Login to ME as Admin user");
			test.assignCategory("regression");	
			driver = driverObj.getEventDriver(webdriver, test);
			
			loginObj = new LoginPage(driver);
			mfaObj = new MFA(driver);
			clientSelectionObj = new ClientSelectionPage(driver);
			dashboardObj = new DashboardPage(driver);
			
			String username = props.getProperty("MobilityExchange_AdminUsername");
			String password = props.getProperty("MobilityExchange_AdminPassword");
			loginObj.login(username,password);					
			if(driver.getPageSource().contains("Verify your identity")){
				mfaObj.authenticate();
			}
			assertTrue(isElementExisting(driver,selectCustomer,40),"Account selection page did not load",test);
			clientSelectionObj.selectCustomer("MERCER");
			clientSelectionObj.selectAccount("97216906 GHRM Clients");
			clientSelectionObj.clickOnContinue();
			waitForElementToDisplay(loggedInAccountNo);
			verifyElementTextContains(loggedInAccountNo,"97216906 GHRM Clients",test);
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
	}
	
	@Test(priority = 2, enabled = false)
	public void users() throws Exception {
		try {
			test = reports.createTest("Verifying users");
			test.assignCategory("regression");
			driver = driverObj.getEventDriver(webdriver, test);
			dnnpageObj = new DNNPage(driver);
			UsersPage usersobj = new UsersPage(driver);
			dnnpageObj.open("Manage", "Users");
			verifyElementTextContains(usersPageHeader,"Users",test);
			usersobj.addNew("Testing", "QA", "testingQA@mercer.com", "testingQA@mercer.com", "Testing@123");
			assertTrue(isElementExisting(driver,createdUserNotification,5),"User creation failed",test);
			usersobj.update("Testing QA", "Mercer");
			usersobj.delete("Testing QA");
			assertTrue(isElementExisting(driver,deletedUserNotification,5),"User deletion failed",test);
			usersobj.permanentlyDelete("Testing QA");
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		}
	
	@Test(priority = 3, enabled = false)
	public void roles() throws Exception {
		try {
			test = reports.createTest("Verifying roles");
			test.assignCategory("regression");
			driver = driverObj.getEventDriver(webdriver, test);
			dnnpageObj = new DNNPage(driver);
			RolesPage rolesobj = new RolesPage(driver);
			dnnpageObj.open("Manage", "Roles");
			verifyElementTextContains(rolesPageHeader,"Roles",test);
			rolesobj.addNew("Testing");
			assertTrue(isElementExisting(driver,createdRoleNotification,5),"Role creation failed",test);
			rolesobj.update("Testing", "Test Update");
			assertTrue(isElementExisting(driver,updatedRoleNotification,5),"Role updation failed",test);
			rolesobj.delete("Testing");
			assertTrue(isElementExisting(driver,deletedRoleNotification,5),"Role deletion failed",test);
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		}
	
	@Test(priority = 4, enabled = false)
	public void announcements() throws Exception {
		try {
			test = reports.createTest("Verifying announcements");
			test.assignCategory("regression");
			driver = driverObj.getEventDriver(webdriver, test);
			dnnpageObj = new DNNPage(driver);
			AnnouncementsPage announcementobj = new AnnouncementsPage(driver);
			dnnpageObj.open("Manage", "Mercer", "Announcements");
			verifyElementTextContains(pageHeader,"Announcements",test);
			announcementobj.addNew("Testing", "02/01/2019", "02/01/2021");
			assertTrue(isElementExisting(driver,savedSitewideNotification,5),"Sitewide announcement creation failed",test);
			announcementobj.updateLink("Testing", "https://www.yahoo.com");
			assertTrue(isElementExisting(driver,savedSitewideNotification,5),"Sitewide announcement updation failed",test);
			announcementobj.delete("Testing");
			assertTrue(isElementExisting(driver,deletedSitewideNotification,5),"Sitewide announcement deletion failed",test);
			announcementobj.selectAnnouncementType("Targeted Notifications");
			announcementobj.addNew("Testing", "02/01/2019", "02/01/2021","GHRM");
			assertTrue(isElementExisting(driver,savedTargetedNotification,5),"Targeted Notifications creation failed",test);
			announcementobj.updateLink("Testing", "https://www.yahoo.com");
			assertTrue(isElementExisting(driver,savedTargetedNotification,5),"Targeted Notifications updation failed",test);
			announcementobj.delete("Testing");
			assertTrue(isElementExisting(driver,deletedTargetedNotification,5),"Targeted Notifications deletion failed",test);
			announcementobj.selectAnnouncementType("Surveys");
			announcementobj.addNew("Testing","https://www.google.com", "02/01/2019", "02/01/2021","GHRM");
			assertTrue(isElementExisting(driver,savedSurveyNotification,5),"Survey creation failed",test);
			announcementobj.updateLink("Testing", "https://www.yahoo.com");
			assertTrue(isElementExisting(driver,savedSurveyNotification,5),"Survey updation failed",test);
			announcementobj.delete("Testing");
			assertTrue(isElementExisting(driver,deletedTargetedNotification,5),"Survey deletion failed",test);
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		}
	@Test(priority = 5, enabled = false)
	public void events() throws Exception {
		try {
			test = reports.createTest("Verifying events");
			test.assignCategory("regression");
			driver = driverObj.getEventDriver(webdriver, test);
			dnnpageObj = new DNNPage(driver);
			EventsPage eventsobj = new EventsPage(driver);
			dnnpageObj.open("Manage", "Mercer", "Events");
			verifyElementTextContains(pageHeader,"Events",test);
			eventsobj.addNew("Testing", "Training", "Asia", "02/01/2019", "23:00", "https://www.google.com", "Test", "Testing");
			assertTrue(isElementExisting(driver,createdEventNotification,5),"Event creation failed",test);
			eventsobj.update("Testing", "Test Update");
			assertTrue(isElementExisting(driver,updatedEventNotification,5),"Event updation failed",test);
			eventsobj.delete("Testing");
			assertTrue(isElementExisting(driver,deletedEventNotification,5),"Event deletion failed",test);
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		}
	@Test(priority = 6, enabled = false)
	public void webcasts() throws Exception {
		try {
			test = reports.createTest("Verifying webcasts");
			test.assignCategory("regression");
			driver = driverObj.getEventDriver(webdriver, test);
			dnnpageObj = new DNNPage(driver);
			WebcastsPage webcastsobj = new WebcastsPage(driver);
			dnnpageObj.open("Manage", "Mercer", "Webcasts");
			verifyElementTextContains(pageHeader,"Webcasts",test);
			webcastsobj.addNew("Testing", "02/01/2019", "testing@123.com", "https://www.google.com", "Test");
			assertTrue(isElementExisting(driver,createdWebcastNotification,5),"Webcast creation failed",test);
			webcastsobj.update("Testing", "Test Update");
			assertTrue(isElementExisting(driver,updatedWebcastNotification,5),"Webcast updation failed",test);
			webcastsobj.delete("Testing");
			assertTrue(isElementExisting(driver,deletedWebcastNotification,5),"Webcast deletion failed",test);
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		}
	@Test(priority = 7, enabled = false)
	public void links() throws Exception {
		try {
			test = reports.createTest("Verifying links");
			test.assignCategory("regression");
			driver = driverObj.getEventDriver(webdriver, test);
			dnnpageObj = new DNNPage(driver);
			LinksPage linksobj = new LinksPage(driver);
			dnnpageObj.open("Manage", "Mercer", "Links");
			verifyElementTextContains(pageHeader,"Links",test);
			linksobj.addNew("Testing", "Testing", "https://www.google.com", "GHRM", "Housing");
			assertTrue(isElementExisting(driver,savedLinkNotification,5),"Link creation failed",test);
			linksobj.update("Testing", "Test Update");
			assertTrue(isElementExisting(driver,savedLinkNotification,5),"Link updation failed",test);
			linksobj.delete("Testing");
			assertTrue(isElementExisting(driver,deletedLinkNotification,5),"Link deletion failed",test);
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		}
	@Test(priority = 8, enabled = false)
	public void applicationSettings() throws Exception {
		try {
			test = reports.createTest("Verifying application settings");
			test.assignCategory("regression");
			driver = driverObj.getEventDriver(webdriver, test);
			dnnpageObj = new DNNPage(driver);
			ApplicationSettingsPage settingsobj = new ApplicationSettingsPage(driver);
			dnnpageObj.open("Manage", "Mercer", "Application Settings");
			verifyElementTextContains(pageHeader,"Application Settings",test);
			settingsobj.edit("2020");
			assertTrue(isElementExisting(driver,updatedSettingsNotification,5),"Application Settings updation failed",test);
			settingsobj.edit("2017");
			assertTrue(isElementExisting(driver,updatedSettingsNotification,5),"Application Settings updation failed",test);
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		}
	@Test(priority = 9, enabled = false)
	public void productCatalogConfig() throws Exception {
		try {
			test = reports.createTest("Verifying product catalog configuration ");
			test.assignCategory("regression");
			driver = driverObj.getEventDriver(webdriver, test);
			dnnpageObj = new DNNPage(driver);
			ProductCatalogConfigPage configobj = new ProductCatalogConfigPage(driver);
			dnnpageObj.open("Manage", "Mercer", "Product Catalog Configuration");
			verifyElementTextContains(pageHeader,"Product Catalog Configuration",test);
			configobj.addNew("Currency Outlook", "Home");
			assertTrue(isElementExisting(driver,savedPCCNotification,5),"Product catalog configuration creation failed",test);
			configobj.update("Currency Outlook", "Host");
			assertTrue(isElementExisting(driver,savedPCCNotification,5),"Product catalog configuration updation failed",test);
			configobj.delete("Currency Outlook");
			assertTrue(isElementExisting(driver,deletedPCCNotification,5),"Product catalog configuration deletion failed",test);
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		}
	@Test(priority = 10, enabled = true)
	public void LocationTypeProduct() throws Exception {
		try {
			test = reports.createTest("Verifying product catalog creation for location type product");
			test.assignCategory("regression");
			driver = driverObj.getEventDriver(webdriver, test);
			dnnpageObj = new DNNPage(driver);
			initialize();
			dnnpageObj.open("Manage", "Mercer","Product Catalog Management");
			waitForElementToDisplay(catalogPageHeader);
			verifyElementTextContains(catalogPageHeader,"Product Catalog Management",test);
			catalogMgmtObj.open("Product Catalog Creation");
			catalogCreationObj.createNew("Cost Of Living", "03/01/2017", "2035", "GHRM");
			catalogCreationObj.addProductDetails("COST OF LIVING", "Cost Of Living Report", "CostofLiving.html", "COL_web.jpg", "300");
			catalogCreationObj.addPrice("USD", "1", "600");
			catalogCreationObj.addPrice("EUR", "1", "450");
			catalogCreationObj.addLocations(10);
			catalogCreationObj.selectAllOtherAttributes();
			catalogCreationObj.createCatalog();
			assertTrue(isElementExisting(driver,progressBar,5),"Catalog was not created",test);
			try {
				waitForElementToDisplay(continueButton,driver,80);
				clickElement(continueButton);
				waitForPageLoad(driver);
				}catch(Exception e) {
					dnnpageObj.open("Manage", "Mercer","Product Catalog Management");
					waitForElementToDisplay(catalogPageHeader);
				}
			catalogMgmtObj.open("Product Catalog Creation");
			catalogCreationObj.createFromExisting("Cost Of Living", "03/01/2018", "2036","2035", "GHRM","COST OF LIVING");
			catalogCreationObj.selectAllLocations();
			catalogCreationObj.createCatalog();
			assertTrue(isElementExisting(driver,progressBar,5),"Catalog was not created",test);
			try {
			waitForElementToDisplay(continueButton,driver,80);
			clickElement(continueButton);
			waitForPageLoad(driver);
			}catch(Exception e) {
				dnnpageObj.open("Manage", "Mercer","Product Catalog Management");
				waitForElementToDisplay(catalogPageHeader);
			}
			catalogMgmtObj.open("Publish/Un-Publish");
			publishUnpublishObj.productCatalogDetails("Cost Of Living", "2035", "GHRM", "COST OF LIVING");
			publishUnpublishObj.publish(5);
			assertTrue(isElementExisting(driver,publishNotification,5),"Selected Locations were not published",test);
			publishUnpublishObj.unpublish(2);
			assertTrue(isElementExisting(driver,unpublishedNotification,5),"Selected Locations were not unpublished",test);
			FileDownloader downloadTestFile = new FileDownloader(driver);
	        String downloadedFileAbsoluteLocation = downloadTestFile.downloadFile(export);
	        
	        System.out.println(downloadedFileAbsoluteLocation);
	        assertTrue(new File(downloadedFileAbsoluteLocation).exists(),"File download failed",test);
	        verifyEquals(downloadTestFile.getHTTPStatusOfLastDownloadAttempt(), 200, test);
			publishUnpublishObj.deleteLocation(9);
			assertTrue(isElementExisting(driver,deletedNotification,5),"Selected Locations were not deleted",test);
			
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		}

	@Test(priority = 11, enabled = false)
	public void DistrictTypeProduct() throws Exception {
		try {
			test = reports.createTest("Verifying product catalog creation for Country/district type product");
			test.assignCategory("regression");
			driver = driverObj.getEventDriver(webdriver, test);
			dnnpageObj = new DNNPage(driver);
			initialize();
			dnnpageObj.open("Manage", "Mercer","Product Catalog Management");
			waitForElementToDisplay(catalogPageHeader);
			verifyElementTextContains(catalogPageHeader,"Product Catalog Management",test);
			catalogMgmtObj.open("Product Catalog Creation");
			//waitForElementToDisplay(catalogPageHeader);
			//verifyElementTextContains(catalogPageHeader,"Product Catalog Creation",test);
			catalogCreationObj.createNew("Personal Income Tax", "03/01/2016", "2035", "BOTH");
			catalogCreationObj.addProductDetails("PERSONAL INCOME TAX", "Personal Income Tax", "PersonalIncomeTax.html", "PersonalTaxReports_web.jpg", "329");
			catalogCreationObj.addPrice("USD", "1", "600");
			catalogCreationObj.addPrice("EUR", "1", "450");
			catalogCreationObj.addLocations(20);
			catalogCreationObj.selectAllOtherAttributes();
			catalogCreationObj.createCatalog();
			assertTrue(isElementExisting(driver,progressBar,5),"Catalog was not created",test);
			try {
				waitForElementToDisplay(continueButton,driver,80);
				clickElement(continueButton);
				waitForPageLoad(driver);
				}catch(Exception e) {
					dnnpageObj.open("Manage", "Mercer","Product Catalog Management");
					waitForElementToDisplay(catalogPageHeader);
				}
			catalogMgmtObj.open("Product Catalog Creation");
			catalogCreationObj.createFromExisting("Personal Income Tax", "03/01/2017", "2036","2035", "BOTH","PERSONAL INCOME TAX");
			catalogCreationObj.selectAllLocations();
			catalogCreationObj.createCatalog();
			assertTrue(isElementExisting(driver,progressBar,5),"Catalog was not created",test);
			try {
				waitForElementToDisplay(continueButton,driver,80);
				clickElement(continueButton);
				waitForPageLoad(driver);
				}catch(Exception e) {
					dnnpageObj.open("Manage", "Mercer","Product Catalog Management");
					waitForElementToDisplay(catalogPageHeader);
				}
			catalogMgmtObj.open("Publish/Un-Publish");
			publishUnpublishObj.productCatalogDetails("Personal Income Tax", "2035", "BOTH", "PERSONAL INCOME TAX");
			publishUnpublishObj.publish(5);
			assertTrue(isElementExisting(driver,publishNotification,5),"Selected Country/Districts were not published",test);
			publishUnpublishObj.unpublish(2);
			assertTrue(isElementExisting(driver,unpublishedNotification,5),"Selected Country/Districts were not unpublished",test);
			FileDownloader downloadTestFile = new FileDownloader(driver);
	        String downloadedFileAbsoluteLocation = downloadTestFile.downloadFile(export);	        
	        System.out.println(downloadedFileAbsoluteLocation);
	        assertTrue(new File(downloadedFileAbsoluteLocation).exists(),"File download failed",test);
	        verifyEquals(downloadTestFile.getHTTPStatusOfLastDownloadAttempt(), 200, test);
			publishUnpublishObj.deleteLocation(10);
			assertTrue(isElementExisting(driver,deletedNotification,5),"Selected Country/Districts were not deleted",test);
			
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		}
	@Test(priority = 12, enabled = false)
	public void CountryTypeProduct() throws Exception {
		try {
			test = reports.createTest("Verifying product catalog creation for Country type product");
			test.assignCategory("regression");
			driver = driverObj.getEventDriver(webdriver, test);
			dnnpageObj = new DNNPage(driver);
			initialize();
			dnnpageObj.open("Manage", "Mercer","Product Catalog Management");
			waitForElementToDisplay(catalogPageHeader);
			verifyElementTextContains(catalogPageHeader,"Product Catalog Management",test);
			catalogMgmtObj.open("Product Catalog Creation");
			//waitForElementToDisplay(catalogPageHeader);
			//verifyElementTextContains(catalogPageHeader,"Product Catalog Creation",test);
			catalogCreationObj.createNew("Car Norms", "03/01/2017", "2035", "ICS");
			catalogCreationObj.addProductDetails("CAR COSTS", "Car Norms", "Car-Norms.html", "Car-Norms.jpg", "128");
			catalogCreationObj.addPrice("USD", "1", "600");
			catalogCreationObj.addPrice("EUR", "1", "450");
			catalogCreationObj.addLocations(20);
			catalogCreationObj.createCatalog();
			assertTrue(isElementExisting(driver,progressBar,5),"Catalog was not created",test);
			try {
				waitForElementToDisplay(continueButton,driver,80);
				clickElement(continueButton);
				waitForPageLoad(driver);
				}catch(Exception e) {
					dnnpageObj.open("Manage", "Mercer","Product Catalog Management");
					waitForElementToDisplay(catalogPageHeader);
				}
			catalogMgmtObj.open("Product Catalog Creation");
			catalogCreationObj.createFromExisting("Car Norms", "03/01/2018", "2036","2035", "ICS","CAR COSTS");
			catalogCreationObj.selectAllLocations();
			catalogCreationObj.createCatalog();
			assertTrue(isElementExisting(driver,progressBar,5),"Catalog was not created",test);
			try {
				waitForElementToDisplay(continueButton,driver,80);
				clickElement(continueButton);
				waitForPageLoad(driver);
				}catch(Exception e) {
					dnnpageObj.open("Manage", "Mercer","Product Catalog Management");
					waitForElementToDisplay(catalogPageHeader);
				}
			catalogMgmtObj.open("Publish/Un-Publish");
			publishUnpublishObj.productCatalogDetails("Car Norms", "2035", "ICS", "CAR COSTS");
			publishUnpublishObj.publish(3);
			assertTrue(isElementExisting(driver,publishNotification,5),"Selected Countries were not published",test);
			publishUnpublishObj.unpublish(2);
			assertTrue(isElementExisting(driver,unpublishedNotification,5),"Selected Countries were not unpublished",test);
			FileDownloader downloadTestFile = new FileDownloader(driver);
	        String downloadedFileAbsoluteLocation = downloadTestFile.downloadFile(export);
	        
	        System.out.println(downloadedFileAbsoluteLocation);
	        assertTrue(new File(downloadedFileAbsoluteLocation).exists(),"File download failed",test);
	        verifyEquals(downloadTestFile.getHTTPStatusOfLastDownloadAttempt(), 200, test);
			publishUnpublishObj.deleteLocation(10);
			assertTrue(isElementExisting(driver,deletedNotification,5),"Selected Countries were not deleted",test);
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		}
	
	@Test(priority = 13, enabled = false)
	public void NoneTypeProduct() throws Exception {
		try {
			
			test = reports.createTest("Verifying product catalog creation for None type product");
			test.assignCategory("regression");
			driver = driverObj.getEventDriver(webdriver, test);
			dnnpageObj = new DNNPage(driver);
			initialize();
			dnnpageObj.open("Manage", "Mercer","Product Catalog Management");
			//waitForElementToDisplay(catalogPageHeader);
			//verifyElementTextContains(catalogPageHeader,"Product Catalog Management",test);
			catalogMgmtObj.open("Product Catalog Creation");
			waitForElementToDisplay(catalogPageHeader);
			verifyElementTextContains(catalogPageHeader,"Product Catalog Creation",test);
			catalogCreationObj.createNew("Compensation Localizer", "03/01/2017", "2035", "BOTH");
			catalogCreationObj.addProductDetails("COMPENSATION LOCALIZER", "Compensation Localizer", "Compensation-Localizer.html", "CompLocalizer_web.jpg", "377");
			catalogCreationObj.addPrice("USD", "1", "600");
			catalogCreationObj.addPrice("EUR", "1", "450");
			catalogCreationObj.selectAllOtherAttributes();
			catalogCreationObj.createCatalog();
			assertTrue(isElementExisting(driver,progressBar,5),"Catalog was not created",test);
			try {
				waitForElementToDisplay(continueButton,driver,80);
				clickElement(continueButton);
				waitForPageLoad(driver);
				}catch(Exception e) {
					dnnpageObj.open("Manage", "Mercer","Product Catalog Management");
					waitForElementToDisplay(catalogPageHeader);
				}
			catalogMgmtObj.open("Product Catalog Creation");
			catalogCreationObj.createFromExisting("Compensation Localizer", "03/01/2018", "2036","2035", "BOTH","COMPENSATION LOCALIZER");
			catalogCreationObj.createCatalog();
			assertTrue(isElementExisting(driver,progressBar,5),"Catalog was not created",test);
			try {
				waitForElementToDisplay(continueButton,driver,80);
				clickElement(continueButton);
				waitForPageLoad(driver);
				}catch(Exception e) {
					dnnpageObj.open("Manage", "Mercer","Product Catalog Management");
					waitForElementToDisplay(catalogPageHeader);
				}
			catalogMgmtObj.open("Publish/Un-Publish");
			publishUnpublishObj.productCatalogDetails("Compensation Localizer", "2035", "BOTH", "COMPENSATION LOCALIZER");
			publishUnpublishObj.publish(1);
			assertTrue(isElementExisting(driver,publishNotification,5),"Product was not published",test);
			publishUnpublishObj.unpublish(1);
			assertTrue(isElementExisting(driver,unpublishedNotification,5),"Product was not unpublished",test);
			FileDownloader downloadTestFile = new FileDownloader(driver);
	        String downloadedFileAbsoluteLocation = downloadTestFile.downloadFile(export);        
	        System.out.println(downloadedFileAbsoluteLocation);
	        assertTrue(new File(downloadedFileAbsoluteLocation).exists(),"File download failed",test);
	        verifyEquals(downloadTestFile.getHTTPStatusOfLastDownloadAttempt(), 200, test);
			publishUnpublishObj.deleteLocation(1);
			assertTrue(isElementExisting(driver,deletedNotification,5),"Product was not deleted",test);
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		}
	
	@Test(priority = 14, enabled = false)
	public void deleteProductsByYear() throws Exception {
		try {
			test = reports.createTest("Verifying deletion of products catalog based on the specified year");
			test.assignCategory("regression");
			driver = driverObj.getEventDriver(webdriver, test);
			dnnpageObj = new DNNPage(driver);
			DeleteProductsPage deleteobj = new DeleteProductsPage(driver);
			dnnpageObj.open("Manage", "Mercer","Clear Catalog");
			verifyElementTextContains(pageHeader,"Product Catalog Management",test);
			deleteobj.delete("Cost Of Living", "2035");
			assertTrue(isElementExisting(driver,deletedNotification,20),"Cost Of Living Product for the year 2035 was not deleted",test);
			deleteobj.delete("Cost Of Living", "2036");
			assertTrue(isElementExisting(driver,deletedNotification,20),"Cost Of Living Product for the year 2036 was not deleted",test);
			
			deleteobj.delete("Personal Income Tax", "2035");
			assertTrue(isElementExisting(driver,deletedProductNotification,20),"Personal Income Tax Product for the year 2035 was not deleted",test);
			
			deleteobj.delete("Personal Income Tax", "2036");
			assertTrue(isElementExisting(driver,deletedProductNotification,20),"Personal Income Tax Product for the year 2036 was not deleted",test);
			
			deleteobj.delete("Car Norms", "2035");
			assertTrue(isElementExisting(driver,deletedProductNotification,20),"Car Norms Product for the year 2035 was not deleted",test);
			
			deleteobj.delete("Car Norms", "2036");
			assertTrue(isElementExisting(driver,deletedProductNotification,20),"Car Norms Product for the year 2036 was not deleted",test);
			
			deleteobj.delete("Compensation Localizer", "2035");
			assertTrue(isElementExisting(driver,deletedProductNotification,20),"Compensation Localizer Product for the year 2035 was not deleted",test);
			
			deleteobj.delete("Compensation Localizer", "2036");
			assertTrue(isElementExisting(driver,deletedProductNotification,20),"Compensation Localizer Product for the year 2036 was not deleted",test);

			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		}
	@AfterTest 
	public void close(){
		reports.flush();
		driver.quit();
		killBrowserExe(BROWSER_TYPE);
	}

}
