package pages.mobilityportal;

import static driverfactory.Driver.waitForElementToDisplay;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.setInput;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;

public class LoginPage {


		@FindBy(id = "ctl00_MainSectionContent_Email")
		public WebElement winSTSemail;

		
		@FindBy(id = "ContentPlaceHolder1_PassiveSignInButton")
		public WebElement nextButton;
		
		@FindBy(id = "txtEmail")
		public WebElement mssoEmail;
		
		@FindBy(id="txtPassword-clone")
		public WebElement mssoPasswordHidden;
	
		@FindBy(xpath="//*[@id='txtPassword']")
		public WebElement mssoPassword;
		
		
		@FindBy(xpath = "(//span[contains(text(),'Logout')])[3]//parent::a")
		public WebElement logoutLnk;

		@FindBy(id = "ctl00_MainSectionContent_Password")
		public WebElement winSTSpassword;
		
		@FindBy(xpath = "//*[contains(text(),'Enter')]")
		public WebElement enter;

		// a[@class = 'btn action my-account-lnk dropdown-toggle']
		@FindBy(xpath = "(//span[text()='My Account '])[1]")
		public WebElement myAccounts;

		@FindBy(xpath = "(//span[contains(text(),'Login')])[3]//parent::a")
		public WebElement login;
		
		@FindBy(xpath = "(//i[@class='top-avatar-icon top-avatar'])[3]//parent::a")
		public WebElement userIcon;

		@FindBy(id = "ctl00_MainSectionContent_ButtonSignin")
		public WebElement signIn;

		@FindBy(css = "h1.title-my-tools")
		public WebElement myToolsTxt;
		WebDriver driver;
		public LoginPage(WebDriver driver) {
			this.driver=driver;
			PageFactory.initElements(driver, this);
		}
		
		private void signIn(String username, String passwordTxt) throws InterruptedException {
			try {
					waitForElementToDisplay(nextButton, driver, 20);
					clickElement(nextButton);
					waitForElementToDisplay(mssoEmail);
					setInput(mssoEmail,username);
					clickElement(mssoPasswordHidden);
					setInput(mssoPassword,passwordTxt);
					clickElement(enter);	
			}
			catch(Exception e1) {
				waitForElementToDisplay(winSTSemail);
				setInput(winSTSemail,username);
				setInput(winSTSpassword,passwordTxt);
				clickElement(signIn);
			}
				
		}

		public void login(String username, String passwordTxt) throws Exception {
			/*
			 * try catch logic to handle IE issue
			 * redirection to Home page instead of login page when previous scripts fails before logout
			 */
			try
			{
				clickElementUsingJavaScript(driver,userIcon);
				waitForElementToDisplay(logoutLnk,driver,5);
				clickElement(logoutLnk);
				clickElement(userIcon);
				clickElement(login);
				signIn(username,passwordTxt);
				
			}
			catch(Exception e)
			{
				clickElement(login);
				signIn(username,passwordTxt);
			}		

		}
		
		
	}

