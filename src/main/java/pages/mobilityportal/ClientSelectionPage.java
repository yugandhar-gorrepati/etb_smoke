package pages.mobilityportal;

import static driverfactory.Driver.*;
import static driverfactory.Driver.getEleByXpathContains;

import static driverfactory.Driver.refreshpage;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ClientSelectionPage {
WebDriver driver;
	

public static  WebElement customer;

public static  WebElement account;

@FindBy (xpath = "//h3[contains(text(),'Select a customer:')]")
public static WebElement selectCustomer;

@FindBy(xpath = "//input[contains(@data-ng-change,'searchClient')]")
WebElement searchBox;

@FindBy(xpath="//button[contains(text(),'Continue')]")
public static  WebElement continueBtn;
 
	public ClientSelectionPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void selectCustomer(String customerTxt) throws InterruptedException {
		try {
		refreshpage();
		customer=getEleByXpathContains("a",customerTxt,driver);
		clickElement(customer);
		}catch(Exception e) {
			setInput(searchBox,customerTxt+Keys.ENTER);
			delay(3000);
			customer=driver.findElement(By.xpath("//a[contains(text(),'"+customerTxt+"')][1]"));
			clickElement(customer);
		}
	}
	

	public void selectAccount(String accountTXt) throws InterruptedException {
		account=getEleByXpathContains("a",accountTXt,driver);
		clickElement(account);
		
	}
	public void clickOnContinue() throws InterruptedException
	{
		clickElement(continueBtn);
		
	}
	
}

