package pages.mobilityexchange;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;

public class ProductCatalogConfigPage {
	WebDriver driver;
	@FindBy(xpath = "//a[contains(text(),'Add New Record')]")
	WebElement addNewRecord;
	@FindBy(xpath = "//label[contains(text(),'Product')]//following::select[@name='product']")
	WebElement product;
	@FindBy(xpath = "//label[contains(text(),'Location Type')]//following::select[@name='locationType']")
	WebElement locationType;
	@FindBy(xpath ="//a[contains(text(),'Save')]")
	WebElement save;
	@FindBy(xpath = "//a[contains(text(),'2')]")
	WebElement secondPage;
	@FindBy(xpath = "//span[contains(text(),'Product catalog configuration saved successfully')]")
	public static WebElement savedPCCNotification;
	@FindBy(xpath = "//span[contains(text(),'Record deleted successfully')]")
	public static WebElement deletedPCCNotification;
	
	
	public ProductCatalogConfigPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	public void addNew(String product,String locationType) throws InterruptedException {
		clickElement(addNewRecord);
		selEleByVisbleText(this.product,product);
		selEleByVisbleText(this.locationType,locationType);
		clickElement(save);
	}
	public void update(String product,String locationType) throws InterruptedException {
		waitForElementToEnable(secondPage);
		clickElement(secondPage);
		clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//td[contains(text(),'"+product+"')]//preceding::button[contains(text(),'Edit')][1]")));
		selEleByVisbleText(this.locationType,locationType);
		clickElement(save);
	}
	public void delete(String product) throws InterruptedException {
		clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//td[contains(text(),'"+product+"')]//preceding::button[contains(text(),'Delete')][1]")));
		driver.switchTo().alert().accept();
	}
}
