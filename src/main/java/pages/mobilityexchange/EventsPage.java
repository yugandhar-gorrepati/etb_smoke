package pages.mobilityexchange;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;

public class EventsPage {
	WebDriver driver;
	@FindBy(xpath = "//a[contains(text(),'Add New Event')]")
	WebElement addNewEvent;
	@FindBy(xpath = "//label[contains(text(),'Title')]//following::input[@id='title']")
	WebElement titleInput;
	@FindBy(xpath ="//a[contains(text(),'Save')]")
	WebElement save;
	@FindBy(xpath ="//select[@name='eventType']")
	WebElement eventType;
	@FindBy(xpath ="//select[@name='location']")
	WebElement location;
	@FindBy(xpath ="//input[@name='date']")
	WebElement date;
	@FindBy(xpath ="//input[@name='time']")
	WebElement time;
	@FindBy(xpath ="//input[@name='url']")
	WebElement url;
	@FindBy(xpath ="//textarea[@name='shortDescription']")
	WebElement shortDescription;
	@FindBy(xpath ="//body[@contenteditable]")
	WebElement fullDescription;
	@FindBy(xpath = "//span[contains(text(),'Event added successfully')]")
	public static WebElement createdEventNotification;
	@FindBy(xpath = "//span[contains(text(),'Event updated successfully')]")
	public static WebElement updatedEventNotification;
	@FindBy(xpath = "//span[contains(text(),'deleted successfully')]")
	public static WebElement deletedEventNotification;

	public EventsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	public void addNew(String title,String type,String location,String date, String time,String url, String shortDesc, String fullDesc) throws InterruptedException {
		clickElement(addNewEvent);
		setInput(titleInput, title);
		selEleByVisbleText(eventType,type);//Training
		selEleByVisbleText(this.location,location);//Asia
		clickElementUsingJavaScript(driver,this.date);
		setInput(this.date,date);
		setInput(this.time, time);
		setInput(this.url,url);
		setInput(shortDescription,shortDesc);
		switchToFrame(driver,driver.findElement(By.xpath("//iframe[@class]")));
		clickElementUsingJavaScript(driver,fullDescription);
		setInput(fullDescription,fullDesc);
		switchToDefaultContent(driver);
		clickElement(save);
	}
	public void update(String title,String shortDesc) throws InterruptedException {
		clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//td[text()='"+title+"']//following::button[contains(text(),'Edit')][1]")));
		setInput(shortDescription,shortDesc);
		clickElement(save);
	}
	public void delete(String title) throws InterruptedException {
		clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//td[text()='"+title+"']//following::button[contains(text(),'Delete')][1]")));
		driver.switchTo().alert().accept();
	}
}
