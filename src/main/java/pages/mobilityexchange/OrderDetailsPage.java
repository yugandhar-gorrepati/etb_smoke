package pages.mobilityexchange;

import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OrderDetailsPage {
	WebDriver driver;
	@FindBy (xpath = "//div[@id='dnn_ctr555_ModuleContent']//child::h2")
	public static WebElement orderCompleteMessage;
	@FindBy (xpath = "(//a[contains(text(),'Download')])[1]")
	public static WebElement downloadPDF;
	@FindBy(xpath = "//a[contains(text(),'Order History')]")
	WebElement orderHistory;
	@FindBy(xpath = "//div[2]//child::a[contains(text(),'View Details')]")
	WebElement viewDetails;
	@FindBy(xpath = "(//div[contains(@class,'col-xs-6 col-md-4 grayblock titles')])[1]//child::div[1]")
	public static WebElement orderNumberHeading;
	@FindBy(xpath = "(//div[contains(@class,'col-xs-6 col-md-4 grayblock titles')])[1]//child::div[2]")
	public static WebElement orderDateHeading;



	public OrderDetailsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver=driver;
	}
	public void viewPreviousOrder() throws InterruptedException {
		clickElement(viewDetails);
	}
}
