package pages.mobilityexchange;

import static driverfactory.Driver.*;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.switchToFrame;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DNNPage {
	WebDriver driver;
	@FindBy (id = "personaBar-iframe")
	WebElement dNNToolbar;
	@FindBy (xpath = "//h3[contains(text(),'Product Catalog Management')]")
	WebElement productCatalogManagement;
	@FindBy(xpath ="//span[contains(@id,'title')]")
	public static WebElement pageHeader;
	
	public DNNPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	public void open(String menu, String submenu1, String submenu2 ) throws InterruptedException {
		waitForElementToDisplay(dNNToolbar);
		switchToFrame(driver,dNNToolbar);
		hoverOverElement(driver,driver.findElement(By.id(menu)));
		clickElement(driver.findElement(By.xpath("//li[@data-name='"+submenu1+"']")));
		clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//a[@aria-label='"+submenu2+"']")));
	}
	public void open(String menu, String submenu) throws InterruptedException {
		try{
			waitForElementToDisplay(dNNToolbar,driver,5);
			switchToFrame(driver,dNNToolbar);
		}catch(Exception e){
			System.out.println("Already inside the DNN toolbar");
		}
		
		hoverOverElement(driver,driver.findElement(By.id(menu)));
		clickElement(driver.findElement(By.xpath("//li[@data-name='"+submenu+"']")));
	}

}
