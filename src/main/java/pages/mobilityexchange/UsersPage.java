package pages.mobilityexchange;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;

public class UsersPage {
	WebDriver driver;
	@FindBy(xpath = "//button[contains(text(),'Add User')]")
	WebElement addUser;
	@FindBy(xpath = "//label[contains(text(),'First Name')]//following::input[1]")
	WebElement firstName;
	@FindBy(xpath = "//label[contains(text(),'Last Name')]//following::input[1]")
	WebElement lastName;
	@FindBy(xpath = "//span[contains(text(),'Middle Name')]//following::input[1]")
	WebElement middleName;
	@FindBy(xpath = "//label[contains(text(),'User Name')]//following::input[1]")
	WebElement userName;
	@FindBy(xpath = "//label[contains(text(),'Email Address')]//following::input[1]")
	WebElement emailAddress;
	@FindBy(xpath = "//label[text()='Password:']//following::input[1]")
	WebElement password;
	@FindBy(xpath = "//label[contains(text(),'Confirm Password')]//following::input[1]")
	WebElement confirmPassword;
	@FindBy(xpath = "//button[text()='Save']")
	WebElement save;
	@FindBy(xpath = "//a[text()='Update']")
	WebElement update;
	@FindBy(xpath ="//li[contains(text(),'Delete User')]")
	WebElement deleteUser;
	@FindBy(xpath = "//a[text()='Delete']")
	WebElement deleteConfirm;
	@FindBy(xpath = "//iframe[@class='edit-profile']")
	WebElement editProfileFrame;
	@FindBy(xpath = "//div[@class='collapsible-label no-wrap']")
	WebElement usersDropdown;
	@FindBy(xpath = "//div[@class='collapsible-label no-wrap']//following::li[text()='Deleted'][1]")
	WebElement deletedOption;
	@FindBy(xpath = "//li[contains(text(),'Remove User Permanently')]")
	WebElement removeUserPermanently;
	@FindBy(xpath = "(//span[contains(@class,'title')]//child::h3)[1]")
	public static WebElement usersPageHeader;
	@FindBy(xpath = "//p[contains(text(),'User created successfully')]")
	public static WebElement createdUserNotification;
	@FindBy(xpath = "//p[contains(text(),'User deleted successfully')]")
	public static WebElement deletedUserNotification;
	
	public UsersPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	public void addNew(String firstname, String lastname, String username, String email, String password) throws InterruptedException {
		clickElement(addUser);
		setInput(firstName,firstname);
		setInput(lastName,lastname);
		setInput(userName,username);
		setInput(emailAddress,email);
		setInput(this.password,password);
		setInput(confirmPassword,password);
		clickElement(save);
	}
	public void update(String username,String middlename) throws InterruptedException {
		clickElement(driver.findElement(By.xpath("//*[text()='"+username+"']//following::div[@title='Profile Settings'][1]")));
		switchToFrame(driver,editProfileFrame);
		waitForElementToDisplay(middleName);
		setInput(middleName,middlename);
		clickElement(update);
	}
	 public void delete(String username) throws InterruptedException {
		 driver.switchTo().parentFrame();
		 clickElementUsingJavaScript(driver,driver.findElement(By.xpath("(//*[text()='"+username+"']//following::div[contains(@class,'extension-action true')])[1]")));
		 clickElementUsingJavaScript(driver,deleteUser);
		 clickElement(deleteConfirm);
	 }
	public void permanentlyDelete(String username) throws InterruptedException {
		clickElementUsingJavaScript(driver,usersDropdown);
		clickElementUsingJavaScript(driver,deletedOption);
		clickElementUsingJavaScript(driver,driver.findElement(By.xpath("(//*[text()='"+username+"']//following::div[contains(@class,'extension-action true')])[1]")));
		 clickElementUsingJavaScript(driver,removeUserPermanently);
		 clickElement(deleteConfirm);
	}
}
