package pages.mobilityexchange;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.*;

public class PublishUnpublishPage {
	WebDriver driver;
	@FindBy(xpath = "//a[contains(text(),'Publish/Un-Publish')]")
	WebElement publishUnpublish;
	@FindBy(xpath ="//div[contains(text(),'Product')]//following::select[@name='Product']")
	WebElement product;
	@FindBy(xpath = "//div[contains(text(),'Year')]//following::select[@name='Year']")
	WebElement year;
	@FindBy(xpath = "//div[contains(text(),'Methodology')]//following::select[@name='Methodology']")
	WebElement methodology;
	@FindBy(xpath = "//div[contains(text(),'Category')]//following::select[@name='Category']")
	WebElement category;
	@FindBy(xpath ="//a[text()='Show Details' and @ng-click]")
	WebElement showDetails;
	@FindBy(xpath = "//div[contains(text(),'File Retrieved Status')]//following::select[1]")
	WebElement fileRetrievedStatusFilter;
	@FindBy(xpath ="//div[contains(text(),'Published Status')]//following::select")
	WebElement publishedStatusFilter;
	@FindBy(xpath ="//a[text()='Publish']")
	WebElement publish;
	@FindBy(xpath ="//a[text()='Un-Publish']")
	WebElement unPublish;
	@FindBy(xpath ="//a[text()='Delete']")
	WebElement delete;
	@FindBy(xpath ="//button[text()='Export']")
	public static WebElement export;
	@FindBy(xpath = "//span[contains(text(),'File has been Published successfully')]")
	public static WebElement publishNotification;
	@FindBy(xpath = "//span[contains(text(),'File has been UnPublished successfully')]")
	public static WebElement unpublishedNotification;
	@FindBy(xpath = "//span[contains(text(),'deleted successfully')]")
	public static WebElement deletedNotification;
	
	public PublishUnpublishPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	public void productCatalogDetails(String product, String year,String methodology, String category) throws InterruptedException {
		selEleByVisbleText(this.product,product);
		selEleByVisbleText(this.year,year);
		selEleByVisbleText(this.methodology,methodology);
		selEleByVisbleText(this.category,category);
		clickElementUsingJavaScript(driver,showDetails);
	}
	public void publish(int numberOfLocations) throws InterruptedException {
		scrollToElement(driver,fileRetrievedStatusFilter);
		selEleByVisbleText(fileRetrievedStatusFilter,"Yes");
		scrollToElement(driver,publishedStatusFilter);
		selEleByVisbleText(publishedStatusFilter,"UnPublish");
		for(int i=1;i<=numberOfLocations;i++) 
		clickElement(driver.findElement(By.xpath("//tr[@class='ng-scope']["+i+"]//child::span[@class='checkbox']")));
		clickElement(publish);
	}
	public void unpublish(int numberOfLocations) throws InterruptedException {
		scrollToElement(driver,publishedStatusFilter);
		selEleByVisbleText(publishedStatusFilter,"Publish");
		for(int i=1;i<=numberOfLocations;i++) 
		clickElement(driver.findElement(By.xpath("//tr[@class='ng-scope']["+i+"]//child::span[@class='checkbox']")));
		clickElement(unPublish);
	}
	public void deleteLocation(int position) throws InterruptedException {
		clickElement(driver.findElement(By.xpath("//tr[@class='ng-scope'][1]//child::span[@class='checkbox']")));
		clickElement(delete);
		driver.switchTo().alert().accept();
	}
//	public void export() {
//		clickElement(ExportButton);
//	}
}
