package pages.mobilityexchange;

import static driverfactory.Driver.clickElement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyDataTab {
WebDriver driver;
@FindBy (xpath = "//label[contains(text(),'Currently Viewing')]//following::div[contains(@dropdown-onchange,'ProductList')]")
WebElement selectProduct;
@FindBy (xpath = "//div[contains(text(),'Purchased Products')]")
public static WebElement purchasedProducts;
@FindBy (xpath = "//div[contains(text(),'Available for Purchase')]")
public static WebElement availableForPurchase;
@FindBy(xpath = "//li[contains(@class,'item-to-buy')][1]//child::span[contains(@class,'checkbox')]")
WebElement selectProduct1;
@FindBy(xpath = "//li[contains(@class,'item-to-buy')][2]//child::span[contains(@class,'checkbox')]")
WebElement selectProduct2;
@FindBy (xpath = "//a[contains(text(),'add to cart')]")
WebElement addToCart;
	public MyDataTab(WebDriver driver) {
		PageFactory.initElements(driver, this)	;
		this.driver = driver;
		}
	
	public void productSelection(String product) throws InterruptedException {
		clickElement(selectProduct);
		clickElement(driver.findElement(By.partialLinkText(product)));
				
	}
	public void addToCartFromMyDataPage(String product) throws InterruptedException {
		clickElement(selectProduct1);
		clickElement(selectProduct2);
		clickElement(addToCart);
	}
}
