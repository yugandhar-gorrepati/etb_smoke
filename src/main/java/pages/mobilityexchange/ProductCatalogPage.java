package pages.mobilityexchange;

import static driverfactory.Driver.*;
import static driverfactory.Driver.clickElement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductCatalogPage {
WebDriver driver;

@FindBy (xpath = "//table[@id='searchGrid']//following::td[@class='buyCol_D'][1]")
WebElement selectProduct1;
@FindBy (xpath = "//table[@id='searchGrid']//following::td[@class='buyCol_D'][2]")
WebElement selectProduct2;
@FindBy (xpath = "//table[@id='searchGrid']//following::td[@class='buyCol_D'][3]")
WebElement selectProduct3;
@FindBy (xpath = "//a[contains(text(),'add to cart')]")
WebElement addToCart;
@FindBy(xpath = "//span[contains(text(),'items added to your cart')]")
public static WebElement addedToCartNotification;

public ProductCatalogPage(WebDriver driver) {
	PageFactory.initElements(driver, this);
	this.driver=driver;
}

public void purchaseProduct(String category) throws InterruptedException {
delay(5000);
	clickElement(driver.findElement(By.xpath("(//a[contains(@href,'"+category+"')])[1]")));
	scrollToElement(driver,selectProduct1);
	clickElement(selectProduct1);
	if(isElementExisting(driver,selectProduct2,2))
	clickElement(selectProduct2);
	if(isElementExisting(driver,selectProduct3,2))
	clickElement(selectProduct3);
	clickElement(addToCart);
}


}
