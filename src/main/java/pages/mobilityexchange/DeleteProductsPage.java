package pages.mobilityexchange;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;

public class DeleteProductsPage {
	WebDriver driver;
@FindBy(xpath = "//a[contains(text(),'Delete Products By Year')]")
WebElement deleteProductsByYear;
@FindBy(xpath = "//label[text()='Select Product']//following::select[@name='ProductCode']")
WebElement product;
@FindBy(xpath = "//label[text()='Select Product Year']//following::select[@name='ProductYear']")
WebElement productYear;
@FindBy(id ="isElasticServerIndexUpdate")
WebElement runElasticServer;
@FindBy(xpath = "//a[contains(text(),'Delete') and contains(@data-ng-click,'deleteProductsByYear')]")
WebElement delete;
@FindBy(xpath = "//span[contains(text(),'//deleted successfully')]")
public static WebElement deletedProductNotification;

public DeleteProductsPage(WebDriver driver) {
	PageFactory.initElements(driver, this);
	this.driver=driver;
}
public void delete(String product,String year) throws InterruptedException {
	clickElement(deleteProductsByYear);
	selEleByVisbleText(this.product,product);
	selEleByVisbleText(productYear,year);
	clickElement(runElasticServer);
	clickElementUsingJavaScript(driver,delete);
	driver.switchTo().alert().accept();
}
}
