package smoke.mobilityexchange.tests;

import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.switchToWindowByTitle;
import static driverfactory.Driver.waitForElementToDisplay;
import static pages.mobilityexchange.CheckoutPage.checkoutPageHeader;
import static pages.mobilityexchange.MyDataTab.availableForPurchase;
import static pages.mobilityexchange.MyDataTab.purchasedProducts;
import static pages.mobilityexchange.ProductCatalogPage.addedToCartNotification;
import static pages.mobilityportal.ClientSelectionPage.selectCustomer;
import static pages.mobilityportal.DashboardPage.loggedInAccountNo;
import static pages.mobilityportal.HeaderPage.homePageSurvey;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.mobilityexchange.CartPage;
import pages.mobilityexchange.CheckoutPage;
import pages.mobilityexchange.CustomReportTab;
import pages.mobilityexchange.MFA;
import pages.mobilityexchange.MyDataTab;
import pages.mobilityexchange.OrderDetailsPage;
import pages.mobilityexchange.ProductCatalogPage;
import pages.mobilityportal.ClientSelectionPage;
import pages.mobilityportal.DashboardPage;
import pages.mobilityportal.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC2_ICS extends InitTests{
	Driver driverObj = new Driver();
	WebDriver driver=null;
	ExtentTest test=null;
	WebDriver webdriver=null;
	
	LoginPage loginObj;
	MFA mfaObj;
	ClientSelectionPage clientSelectionObj;
	DashboardPage dashboardObj;
	CustomReportTab customReportObj;
	MyDataTab myDataObj;
	ProductCatalogPage productCatalogObj;
	CartPage cartObj;
	CheckoutPage checkoutObj;
	OrderDetailsPage orderDetailsObj;
	

	public TC2_ICS(String appName) {
		super(appName);
	}

	
	@BeforeClass
	public void setUp() throws Exception {
		@SuppressWarnings("unused")
		TC2_ICS obj = new TC2_ICS("MobilityExchange");
//		driverObj = new Driver();
		webdriver = driverObj.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
		
	}

	@Test(priority = 5, enabled = true)
	public void login() throws Exception {
					
			test = reports.createTest("Verifying Login to ME as ICS user");
			test.assignCategory("smoke");
			driver = driverObj.getEventDriver(webdriver, test);
			
			loginObj = new LoginPage(driver);
			mfaObj = new MFA(driver);
			clientSelectionObj = new ClientSelectionPage(driver);
			dashboardObj = new DashboardPage(driver);
			
			try {
				loginObj.login(USERNAME, PASSWORD);
//				if(driver.getPageSource().contains("Verify your identity")){
//					mfaObj.authenticate();
//				}
			assertTrue(isElementExisting(driver,selectCustomer,100),"Account selection page did not load",test);
			clientSelectionObj.selectCustomer("Mercer (New York)");
			clientSelectionObj.selectAccount("11325 Multi-National Pay with Americans Abroad - Catherine Bruning");
			clientSelectionObj.clickOnContinue();
			waitForElementToDisplay(loggedInAccountNo);
			verifyElementTextContains(loggedInAccountNo,"11325 Multi-National Pay with Americans Abroad - Catherine Bruning",test);
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
	}

		
	@Test(priority = 6, enabled = true)
	public void applicationLinks() throws Exception {
		try {
			test = reports.createTest("Verifying links in Dashboard for a ICS user");
			test.assignCategory("smoke");
			driver = driverObj.getEventDriver(webdriver, test);
			
			dashboardObj = new DashboardPage(driver);
			
			dashboardObj.openLink("Compensation Tables");
			assertTrue(driver.getTitle().contains("Compensation Tables"),"Compensation Tables page failed to load",test);
			((JavascriptExecutor)driver).executeScript("window.close()");
			switchToWindowByTitle("Dashboard",driver);
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
	}
	@Test(priority = 7, enabled = true)
	public void myDataTab() throws Exception {
		try {
			test = reports.createTest("Viewing all the products from 'My Data' tab for a ICS user");
			test.assignCategory("smoke");
			driver = driverObj.getEventDriver(webdriver, test);
			
			dashboardObj = new DashboardPage(driver);
			myDataObj = new MyDataTab(driver);
			
			dashboardObj.switchTab("My Data");
			myDataObj.productSelection("Education Report");
			try {
			waitForElementToDisplay(purchasedProducts,driver,100);
			waitForElementToDisplay(availableForPurchase,driver,100);
		}catch (TimeoutException e){
			test.log(Status.FAIL, "Cost Of Living products are not available for the user");
		}
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		}
	
	@Test(priority = 8, enabled = true)
	public void purchase() throws Exception {
		try {
			test = reports.createTest("Verifying product purchase for a ICS user");
			test.assignCategory("smoke");
			driver = driverObj.getEventDriver(webdriver, test);
			
			productCatalogObj = new ProductCatalogPage(driver);
			cartObj = new CartPage(driver);
			checkoutObj = new CheckoutPage(driver);
			orderDetailsObj = new OrderDetailsPage(driver);
			
			dashboardObj.header.navigateLink("Shop", "Product Catalog");
			assertTrue(driver.getTitle().contains("Product Catalog"),"Product Catalog page failed to load",test);
			productCatalogObj.purchaseProduct("LER");
//			assertTrue(isElementExisting(driver,addedToCartNotification,20),"Education Costs products were not added to cart",test);
//	        cartObj.continueShopping();
//	        assertTrue(driver.getTitle().contains("Product Catalog"),"Product Catalog page failed to load",test);
//	        productCatalogObj.purchaseProduct("Personal Income Tax");
//	        assertTrue(isElementExisting(driver,addedToCartNotification,20),"Personal Income Tax products were not added to cart",test);
			cartObj.navigateTocheckout();
			waitForElementToDisplay(checkoutPageHeader);
			verifyElementTextContains(checkoutPageHeader,"Checkout",test);
//			checkoutObj.checkBillingDetails();
//			checkoutObj.placeOrder();
//			verifyElementTextContains(orderCompleteMessage,"Order Received.",test);
			dashboardObj.header.logout();
//			assertTrue(isElementExisting(driver,homePageSurvey,20), "Logged out successfully",test);
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		}

		@AfterClass 
		public void close(){
			reports.flush();
			driver.close();
		}
		

}
