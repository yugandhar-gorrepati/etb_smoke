package smoke.mobilityexchange.tests;

import static driverfactory.Driver.getEleByXpathContains;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.waitForElementToDisplay;
import static pages.mobilityexchange.ProdCatalogMgmtPage.catalogPageHeader;
import static pages.mobilityportal.ClientSelectionPage.selectCustomer;
import static pages.mobilityportal.DashboardPage.loggedInAccountNo;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.mobilityexchange.DNNPage;
import pages.mobilityexchange.MFA;
import pages.mobilityexchange.ProdCatalogCreationPage;
import pages.mobilityexchange.ProdCatalogMgmtPage;
import pages.mobilityexchange.PublishUnpublishPage;
import pages.mobilityportal.ClientSelectionPage;
import pages.mobilityportal.DashboardPage;
import pages.mobilityportal.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC3_Admin extends InitTests {
	WebDriver driver = null;
	Driver driverObj =new Driver();
	ExtentTest test = null;
	WebDriver webdriver = null;
	
	LoginPage loginObj;
	MFA mfaObj;
	ClientSelectionPage clientSelectionObj;
	DashboardPage dashboardObj;
	DNNPage dnnpageObj;
	ProdCatalogMgmtPage catalogMgmtObj;
	ProdCatalogCreationPage catalogCreationObj;
	PublishUnpublishPage publishUnpublishObj;

	public TC3_Admin(String appName) {
		super(appName);
	}

	@BeforeTest
	public void setUp() throws Exception {
		@SuppressWarnings("unused")
		TC3_Admin obj = new TC3_Admin("MobilityExchange");
		driverObj = new Driver();
		webdriver = driverObj.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
	}
		
//	public void initialize() throws Exception {		
//		catalogMgmtObj = new ProdCatalogMgmtPage(driver);
//		catalogCreationObj = new ProdCatalogCreationPage(driver);
//		publishUnpublishObj = new PublishUnpublishPage(driver);
//	}

	
	@Test(priority = 1, enabled = true)
	public void login() throws Exception {
		try {
			test = reports.createTest("Verifying Login to ME as Admin user");
			test.assignCategory("regression");	
			driver = driverObj.getEventDriver(webdriver, test);
			
			loginObj = new LoginPage(driver);
			mfaObj = new MFA(driver);
			clientSelectionObj = new ClientSelectionPage(driver);
			dashboardObj = new DashboardPage(driver);
			
			String username = props.getProperty("MobilityExchange_AdminUsername");
			String password = props.getProperty("MobilityExchange_AdminPassword");
			loginObj.login(username,password);					
			if(driver.getPageSource().contains("Verify your identity")){
				mfaObj.authenticate();
			}
			assertTrue(isElementExisting(driver,selectCustomer,40),"Account selection page did not load",test);
			clientSelectionObj.selectCustomer("MERCER");
			clientSelectionObj.selectAccount("97216906 GHRM Clients");
			clientSelectionObj.clickOnContinue();
			waitForElementToDisplay(loggedInAccountNo);
			verifyElementTextContains(loggedInAccountNo,"97216906 GHRM Clients",test);
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
	}
	@Test(priority = 2, enabled = true)
	public void ProductCatalogManagement() throws Exception {
		try {
			test = reports.createTest("Verifying product catalog creation for location type product");
			test.assignCategory("regression");
			driver = driverObj.getEventDriver(webdriver, test);
			dnnpageObj = new DNNPage(driver);
			dnnpageObj.open("Manage", "Mercer","Product Catalog Management");
			waitForElementToDisplay(catalogPageHeader);
			verifyElementTextContains(catalogPageHeader,"Product Catalog Management",test);
			assertTrue(isElementExisting(driver,getEleByXpathContains("a","Product catalog creation",driver),40),"Product catalog creation button does not exist",test);
			assertTrue(isElementExisting(driver,getEleByXpathContains("a","Publish/Un-Publish",driver),40),"Product catalog creation button does not exist",test);
			
}catch (Error e) {
	e.printStackTrace();
	SoftAssertions.fail(e,
			driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
	ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	softAssert.assertAll();

} catch (Exception e) {
	e.printStackTrace();
	System.out.println("in exception before fail");
	SoftAssertions.fail(e,
		driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
	ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	softAssert.assertAll();
}
}
}
