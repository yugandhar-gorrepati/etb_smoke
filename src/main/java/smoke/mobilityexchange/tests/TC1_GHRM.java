package smoke.mobilityexchange.tests;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import static driverfactory.Driver.*;
import static pages.mobilityexchange.CheckoutPage.checkoutPageHeader;
import static pages.mobilityexchange.MyDataTab.availableForPurchase;
import static pages.mobilityexchange.MyDataTab.purchasedProducts;
import static pages.mobilityexchange.ProductCatalogPage.addedToCartNotification;
import static pages.mobilityportal.ClientSelectionPage.selectCustomer;
import static pages.mobilityportal.DashboardPage.loggedInAccountNo;
import static pages.mobilityportal.HeaderPage.homePageSurvey;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.mobilityexchange.CartPage;
import pages.mobilityexchange.CheckoutPage;
import pages.mobilityexchange.CustomReportTab;
import pages.mobilityexchange.MFA;
import pages.mobilityexchange.MyDataTab;
import pages.mobilityexchange.OrderDetailsPage;
import pages.mobilityexchange.ProductCatalogPage;
import pages.mobilityportal.ClientSelectionPage;
import pages.mobilityportal.DashboardPage;
import pages.mobilityportal.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

public class TC1_GHRM extends InitTests {
	Driver driverObj = new Driver();
	WebDriver driver=null;
	ExtentTest test=null;
	WebDriver webdriver=null;
	
	LoginPage loginObj;
	MFA mfaObj;
	ClientSelectionPage clientSelectionObj;
	DashboardPage dashboardObj;
	CustomReportTab customReportObj;
	MyDataTab myDataObj;
	ProductCatalogPage productCatalogObj;
	CartPage cartObj;
	CheckoutPage checkoutObj;
	OrderDetailsPage orderDetailsObj;
	

	public TC1_GHRM(String appName) {
		super(appName);
	}
	
	@BeforeClass
	public void setUp() throws Exception {
		@SuppressWarnings("unused")
		TC1_GHRM obj = new TC1_GHRM("MobilityExchange");
		webdriver = driverObj.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");	
	}
	
	@Test(priority = 1, enabled = true)
	public void login() throws Exception {
		test = reports.createTest("Verifying Login to ME as a GHRM user");
		test.assignCategory("smoke");	
		driver = driverObj.getEventDriver(webdriver, test);
		
		loginObj = new LoginPage(driver);
		mfaObj = new MFA(driver);
		clientSelectionObj = new ClientSelectionPage(driver);
		dashboardObj = new DashboardPage(driver);
		
		try {
			loginObj.login(USERNAME, PASSWORD);
//			if(driver.getPageSource().contains("Verify your identity")){
//				mfaObj.authenticate();
//			}
			System.out.println("After Logging in");
			assertTrue(isElementExisting(driver,selectCustomer,100),"Account selection page did not load",test);
			clientSelectionObj.selectCustomer("Mercer");
			clientSelectionObj.selectAccount("97216906 GHRM Clients");
			clientSelectionObj.clickOnContinue();
			waitForElementToDisplay(loggedInAccountNo,driver,120);
			verifyElementTextContains(loggedInAccountNo,"97216906 GHRM Clients",test);
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
	}
	
	@Test(priority = 2, enabled = true)
	public void applicationLinks() throws Exception {
		try {
			test = reports.createTest("Verifying links in Dashboard for a GHRM user");
			test.assignCategory("smoke");
			driver = driverObj.getEventDriver(webdriver, test);
			
			dashboardObj = new DashboardPage(driver);
			
			dashboardObj.openLink("Cost of Living Allowance Calculator");
			assertTrue(driver.getTitle().contains("Cost of living allowance calculator"),"Cost of Living page failed to load",test);
			((JavascriptExecutor)driver).executeScript("window.close()");
			switchToWindowByTitle("Dashboard",driver);
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
	}

	@Test(priority = 3, enabled = true)
	public void myDataTab() throws Exception {
		try {
			test = reports.createTest("Viewing all the products from 'My Data' tab for a GHRM user");
			test.assignCategory("smoke");
			driver = driverObj.getEventDriver(webdriver, test);
			
			dashboardObj = new DashboardPage(driver);
			myDataObj = new MyDataTab(driver);
			
			dashboardObj.switchTab("My Data");
			myDataObj.productSelection("Cost Of Living");
			try {
				waitForElementToDisplay(purchasedProducts,driver,100);
				waitForElementToDisplay(availableForPurchase,driver,100);
			}catch (TimeoutException e){
				test.log(Status.FAIL, "Cost Of Living products are not available for the user");	
			}
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
	}
	
	@Test(priority = 4, enabled = true)
	public void purchase() throws Exception {
		try {
			test = reports.createTest("Verifying product purchase for a GHRM user");
			test.assignCategory("smoke");
			driver = driverObj.getEventDriver(webdriver, test);
			
			productCatalogObj = new ProductCatalogPage(driver);
			cartObj = new CartPage(driver);
			checkoutObj = new CheckoutPage(driver);
			orderDetailsObj = new OrderDetailsPage(driver);
			
			dashboardObj.header.navigateLink("Shop", "Product Catalog");
			assertTrue(driver.getTitle().contains("Product Catalog"),"Product Catalog page failed to load",test);
			productCatalogObj.purchaseProduct("COL");
//			assertTrue(isElementExisting(driver,addedToCartNotification,20),"Cost of Living products were not added to cart",test);
//	        cartObj.continueShopping();
//	        assertTrue(driver.getTitle().contains("Product Catalog"),"Product Catalog page failed to load",test);
//	        productCatalogObj.purchaseProduct("Mercer Passport");
//	        assertTrue(isElementExisting(driver,addedToCartNotification,20),"Personal Income Tax products were not added to cart",test);
			cartObj.navigateTocheckout();
			waitForElementToDisplay(checkoutPageHeader);
			verifyElementTextContains(checkoutPageHeader,"Checkout",test);
//			checkoutObj.checkBillingDetails();
//			checkoutObj.placeOrder();
//			verifyElementTextContains(orderCompleteMessage,"Order Received.",test);
			dashboardObj.header.logout();
//			assertTrue(isElementExisting(driver,homePageSurvey,20), "Logged out successfully",test);
		
}catch (Error e) {
e.printStackTrace();
SoftAssertions.fail(e,
		driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
softAssert.assertAll();

} catch (Exception e) {
e.printStackTrace();
System.out.println("in exception before fail");
SoftAssertions.fail(e,
	driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
softAssert.assertAll();
}
}

@AfterClass 
public void close(){
reports.flush();
//driver.quit();
//killBrowserExe(BROWSER_TYPE);
driver.close();
}
}


